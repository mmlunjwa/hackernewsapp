<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/posts/top-stories', 'ItemController@top')->name('top-stories');
Route::get('/posts/new-stories', 'ItemController@new')->name('new-stories');
Route::get('/posts/best-stories', 'ItemController@best')->name('best-stories');
Route::get('/posts', 'ItemController@index')->name('best-stories');

Route::get('/post/show/{id}', 'ItemController@show')->name('post.show');
