<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->BigInteger('id')->unique();
            $table->primary('id');
            $table->string('by');
            $table->BigInteger('parent_id')->nullable()->default(null);
            $table->longText('text');
            $table->string('time');
            $table->string('type');
            $table->bigInteger('commentable_id')->unsigned();
            $table->string('commentable_type');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
