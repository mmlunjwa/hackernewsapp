## Laravel 7 Hacker News APP

The Laravel 7 Hacker news APP is a great way to view all the Hacker news posts and comments for top stories.

Laravel 7 Hacker news APP consumes HackeNews API found hear:  https://github.com/HackerNews/API and does the following:

●	Fetch the news items periodically for top stories, new stories and best stories using a cron schedule setup in app/Console/kernel.php
The following console scripts are scheduled to run every hour:
         $schedule->command('command:top_stories')->hourly();
         $schedule->command('command:best_stories')->hourly();
         $schedule->command('command:new_stories')->hourly();
    The console scripts are located in app/Console/Commands:
    app/Console/Commands/FetchAllStories.php
    app/Console/Commands/FetchBestStories.php
    app/Console/Commands/FetchNewStories.php
    app/Console/Commands/FetchTopStories.php
    
    These commands can be manually run using the following commands:
    
    Top stories:
    run: php artisan command:top_stories
    
    New stories:
    run: php artisan command:new_stories
    
    Best stories:
    run: php artisan command:best_stories
    
    All stories:
    run: php artisan command:all_stories
    
●	Creates and stores all news items into the items sql database table.
●	fetches all the comments associated with the news items recursively and stores them in the comments database table.
●	To view items and comments Navigate to http://127.0.0.1:8000/ this will give you a menu to view stories.
    The menu has Top Stories /posts/top-stories, New Stories /posts/new-stories, Best Stories /posts/best-stories, All Stories /posts/all-stories
    Clicking one of the links will take to the stories listing page that shows the story ID, Title and action.
    To view a story click on show post, this will display all post details and all comments under the /post/show/{id}
●   The view is created using Bootstrap

### Clone the repo
git clone git@bitbucket.org:mmlunjwa/hackernewsapp.git hackernewsapp

cd into hackernewsapp root folder and run: composer update

### Add a database

Create database: hackernews

Edit Database connection details in .env to match your correct DB_USERNAME and DB_PASSWORD

Database connection:
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=hackernews
DB_USERNAME=root
DB_PASSWORD=

### Database migration
run: php artisan migrate

### Run App
php artisan serve

http://127.0.0.1:8000/

### Load stories
cd into hackernewsapp root folder

Top stories:
run: php artisan command:top_stories

New stories:
run: php artisan command:new_stories

Best stories:
run: php artisan command:best_stories

All stories:
run: php artisan command:all_stories

The above commands will:
●	Fetch the news items for top stories, new stories and best stories.
●	Create and store the news item in the hackernews database
●	And fetch all the comments associated with the news item and store them in the hackernews database

To Fetch the news items periodically for top stories, new stories and best stories.
A cron schedule is setup to run every hour. The schedule is set under app/Console/Kernel under the schedule method.
To activate cron schedule run:

php artisan schedule run

### Viewing the stories:
Navigate to http://127.0.0.1:8000/ this will give you a menu to view stories.
The menu has Top Stories /posts/top-stories, New Stories /posts/new-stories, Best Stories /posts/best-stories, All Stories /posts/all-stories
Clicking one of the links will take to the stories listing page that shows the story ID, Title and action.
To view a story click on show post, this will display all post details and all comments under the /post/show/{id}

### PHP Unit tests
To run unit test run: 
./vendor/bin/phpunit

### CI Travis CI
Sets up virtual docker environment
installs PHP 7.3 and MariaDb 10.4
install Composer
use Composer to install PHP dependencies
set environment variables to control which database we use
use Artisan to perform database migrations and start the app in the background
run PHPunit tests
