<!-- _comment_replies.blade.php -->

@foreach($comments as $comment)
    <div class="display-comment">
        <strong>{{ $comment->by }}</strong>
        <p>{{ $comment->text }}</p>
        @include('partials._comment_replies', ['comments' => $comment->replies])
    </div>
@endforeach
