<!-- show.blade.php -->

@extends('layouts.app')
<style>
    .display-comment .display-comment {
        margin-left: 40px
    }
</style>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark justify-content-center">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="{{ url('/')}}">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="{{ url('/posts/top-stories')}}">Top Stories <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ url('/posts/new-stories')}}">New stories</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ url('/posts/best-stories')}}">Best stories</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ url('/posts')}}">All stories</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <p><b>{{ $post->title }}</b></p>
                        <p>
                            Story Link
                            <a class="nav-link" target="_blank" href="{{ $post->url }}">{{ $post->url }}</a>
                        </p>
                        <p>{{"Posted by $post->by "}}{{  gmdate("Y-m-d\ H:i:s", $post->time) }}  </p>
                        <p>{{ "  Score:$post->score " }}  </p>
                        <hr />
                        <h4>Comments</h4>
                        @include('partials._comment_replies', ['comments' => $post->comments, 'post_id' => $post->id])
                        <hr />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
