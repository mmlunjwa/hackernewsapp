<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\ItemController;

class FetchNewStories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:new_stories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch new stories and comments from Hacker News and load them to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $items_controller = new ItemController();
        $items_controller->loadStories(
            http::get('https://hacker-news.firebaseio.com/v0/newstories.json')->json(),
            'New stories'
        );
    }
}
