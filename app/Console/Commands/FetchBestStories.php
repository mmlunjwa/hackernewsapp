<?php

namespace App\Console\Commands;

use App\Http\Controllers\ItemController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class FetchBestStories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:best_stories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch best stories and comments from Hacker News and load them to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $items_controller = new ItemController();
        $items_controller->loadStories(
                http::get('https://hacker-news.firebaseio.com/v0/beststories.json')->json(),
                'Best stories'
        );
    }

}
