<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Item;
use App\comment;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Table;
use PhpParser\Node\Expr\Array_;

/**
 * Class ItemController
 * @package App\Http\Controllers
 */
class ItemController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Item::where('type', '<>', "")->paginate(15);

        return view('index', compact('posts'));
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function new()
    {
        $posts = Item::where('type', 'New stories')->paginate(15);

        return view('new', compact('posts'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function top()
    {
        $posts = Item::where('type', 'Top stories')->paginate(15);

        return view('top', compact('posts'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function best()
    {
        $posts = Item::where('type', 'Best stories')->paginate(15);

        return view('best', compact('posts'));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $post = Item::find($id);

        return view('show', compact('post'));
    }

    /**
     * @param $story
     * @param $story_type
     * return void
     */
    public function loadStories($story, $story_type){
        $loaded_stories = Item::all('id');
        $loaded_ids = array();
        foreach ($loaded_stories as $loaded_story){
            $loaded_ids[] = $loaded_story['id'];
        }

        $top_stories = array_diff($story, $loaded_ids);

        if(!empty($top_stories)){ //Only load new comments
            foreach ($top_stories as $story){
                $story = json_decode(http::get('https://hacker-news.firebaseio.com/v0/item/'.$story.'.json'), true);

                if(isset($story['by']) && isset($story['id']) && isset($story['score']) && isset($story['url']) && isset($story['type']) && isset($story['time']) && isset($story['descendants']) && isset($story['title']) ){//Dont load deleted stories
                    $item = new Item();
                    $item->by = $story['by'];
                    $item->id = $story['id'];
                    $item->score = $story['score'];
                    $item->time = $story['time'];
                    $item->type = $story_type;
                    $item->title = $story['title'];
                    $item->url = $story['url'];
                    $item->descendants = $story['descendants'];

                    $duplicate_item = Item::where('id', $story['id'])->first();
                    if(!$duplicate_item){
                        $item->save();
                        echo  "$story_type id:". $story['id'] ." by". $story['by'] . " loaded successfully! \n";
                    }

                    if(!empty($story['kids'])){
                        $this->saveComments($story['kids'], $story_type); //load comments
                    }
                }
            }
            echo  "$story_type and comments loaded successfully! \n";
        }else{
            echo "No $story_type at the moment \n";
        }
    }

    /**
     * @param $kids
     */
    public function saveComments($kids, $story_type){
            foreach ($kids as $key => $value){
                $comment_item = json_decode(Http::get('https://hacker-news.firebaseio.com/v0/item/'.$value.'.json'), true);

                if(isset($comment_item['by']) && isset($comment_item['id']) && isset($comment_item['parent']) && isset($comment_item['text']) && isset($comment_item['type']) && isset($comment_item['time']) ){ //Dont load deleted comments
                    $comment = new Comment();
                    $comment->by = $comment_item['by'];
                    $comment->id = $comment_item['id'];
                    $comment->parent_id = $comment_item['parent'];
                    $comment->text = $comment_item['text'];
                    $comment->type = $story_type;
                    $comment->time = $comment_item['time'];
                    $comment->commentable_id = $comment_item['parent'];
                    $comment->commentable_type = 'App\Item';

                    $duplicate_comment = Comment::where('id', $comment_item['id'])->first();
                    if(!$duplicate_comment){
                        $comment->save();
                        echo  "Comment id:". $comment_item['id'] ." by". $comment_item['by'] . " loaded successfully! \n";
                    }

                    if (!empty($comment_item['kids'])){
                        $this->saveComments($comment_item['kids'], $story_type);
                    }
                }
                echo  "comments loaded successfully! \n";
            }
    }
}
