<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class comment
 * @package App
 */
class comment extends Model
{
    /**
     * @var string
     */
    protected $table = 'comments';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }
}
