<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\comment;

/**
 * Class item
 * @package App
 */
class item extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(\App\Comment::class, 'commentable')->join('items', 'comments.parent_id', '=', 'items.id');
    }
}
